//var ws = new WebSocket("ws://" + new String(window.location).split('/')[2] + ":3000");
var ws = new WebSocket("ws://localhost:3000");
var chatHistory;
var form;
var input;
var table;
var seat_template;
var seats = [];

// websocket callbacks
ws.onopen = function(e){
    console.log("Connected");
    join();
}

ws.onmessage = function(e){
    var msg = JSON.parse(e.data);
    console.log("Recieved:", msg);

    events[msg.event](msg.data);
}


function join(){
    var msg = {
        'event': 'join',
        'data' : {
            game  : 'test',
            player: 'Abilene',
        }
    }
    ws.send(JSON.stringify(msg));
}

function chatAppend(data){
    var line = data.player + ': ' + data.line;
    chatHistory.value += '\n' + line;
    chatHistory.scrollTop = chatHistory.scrollTopMax;
}

function echo(data){
    console.log(data);
}


function log(data){
    logHistory.value += '\n' + data;
    logHistory.scrollTop = logHistory.scrollTopMax;
}


function sendChat(line){
    if (!line.length) return;
    var msg = {
        'event': 'chat',
        'data' : line,
    }
    ws.send(JSON.stringify(msg));
    console.log("Sent:", msg);
}

function getChatInput(){
    return input.value;
}

function clearChatInput(){
    input.value = '';
}

function chatSubmit(e){
    e.preventDefault();
    console.log(e);

    var line = getChatInput();
    sendChat(line);
    clearChatInput();

    return false;
}


String.prototype.charCodeUTF32 = function(){
    return ((((this.charCodeAt(0)-0xD800)*0x400) + (this.charCodeAt(1)-0xDC00) + 0x10000));
};


function getName(tile){
    var hex  = tile.charCodeUTF32().toString(16).toUpperCase();
    var name = tilemap[hex];
    return name;
}

function tileStringToSpans(str){
    if (!str) return '';
    var html = '';
    for (var j=0; j < str.length; j += 2){
        html = html +
               '<span title="' + getName(str.slice(j, j+2)) + '">' + str.slice(j, j+2) + '</span>';
    }
    return html;
}

function updateSeats(data){
    for (var i=0; i<seats.length; i++){
        var seat = seats[i];

        // update names
        try {
            seat.childNodes[0].innerText = data.seats[i].direction;
            seat.childNodes[1].innerText = data.seats[i].player.name;
        } catch (e) {
            seat.childNodes[0].innerText = '';
            seat.childNodes[1].innerText = '';
        }

        // update tiles
        // discards
        var discard_html = tileStringToSpans(data.seats[i].player.discards);
        if (discard_html){
            seat.childNodes[2].childNodes[0].innerHTML = discard_html;
        } else {
            seat.childNodes[2].childNodes[0].innerHTML = '';
        }

        // melds
        var meld_html = '';
        for (var j=0; j<data.seats[i].player.melds.length; j++){
            var _set = data.seats[i].player.melds[j];
            meld_html = meld_html + tileStringToSpans(_set) + ' ';
        }
        try {
            seat.childNodes[2].childNodes[1].innerHTML = meld_html;
        } catch (e) {
            seat.childNodes[2].childNodes[1].innerHTML = '';
        }

        // hand
        var hand_html = tileStringToSpans(data.seats[i].player.hand);
        try {
            seat.childNodes[2].childNodes[2].innerHTML = hand_html;
        } catch (e) {
            seat.childNodes[2].childNodes[2].innerHTML = '';
        }

        // highlight current seat
        console.log("Active seat: " + data.active_seat);
        if (data.active_seat == i){
            seat.className = 'pane seat active';
        } else {
            seat.className = 'pane seat';
        }
    }
}

function onJoin(data){
    console.log(data);
    window.my_seat = data.seat;
}

function updateState(data){
    console.log(data);
    updateSeats(data);

    if (data.mahjongg){
        alert('mahjongg!');
        console.log(data.mahjongg);
    }

    if (my_seat == data.active_seat){
        alert('You are the active player.');
    }
}

// mapping of event types to callbacks
var events = {
    'chat'  : chatAppend,
    'state' : updateState,
    'info'  : log,
    'warn'  : log,
    'joined': onJoin,
}

// wait for DOM to load and assign form listeners
document.addEventListener("DOMContentLoaded", function(){
    chatHistory = document.getElementById("chatHistory");
    logHistory  = document.getElementById("logHistory");

    form = document.getElementById("chatInput");
    form.addEventListener('submit', chatSubmit);

    input = document.getElementById("chatInputText");

    table = document.getElementById("table");
    seat_template = document.getElementById("seat_template");

    seats = document.getElementsByClassName('seat');
})
