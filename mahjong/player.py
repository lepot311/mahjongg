import abc
import asyncio
from itertools import chain
import logging
import random

from mahjong.exceptions import *
from mahjong.tilecollection import TileCollection


class Player(abc.ABC):
    hints = True

    def __init__(self, name=None):
        self.name     = name
        self.hand     = TileCollection()
        self.discards = TileCollection()
        self.melds    = []
        self.wins     = 0
        self.speed    = 0

    @property
    def to_dict(self):
        return {
            'name': self.name,
            'hand': self.hand.to_dict,
        }

    def seat(self, game):
        seats = [ s.player for s in game.seats ]
        return seats.index(self)

    def join(self, game):
        game.player_join(self)

    def draw(self, game, count=1):
        game.player_draw(self, count)

    def fold(self, game):
        game.player_fold(self)

    def discard(self, game, tile):
        game.player_discard(self, tile)

    def discard_best(self, game):
        try:
            game.player_discard(self, random.choice(self.hand.unused))
        except IndexError:
            game.player_discard(self, random.choice(self.hand))

    def meld(self, game, tiles):
        game.player_meld(self, tiles)

    def call_meld(self, game, tiles, discard):
        try:
            game.player_call_meld(self, tiles, discard)
        except MeldCallError:
            pass

    def mahjongg(self, game, tiles):
        game.player_mahjongg(self, tiles)

    def take_turn(self, game):
        pass


    def get_calls(self, game):
        # call discards
        logging.debug(f"{self.name} checks for calls")

        hint = game.get_hint_call(self)
        if hint:
            _, tiles = hint

            if not tiles:
                return None
            else:
                logging.warn(f"{self.name} gets a call hint: {hint}")
                if isinstance(tiles[0], TileCollection):
                    result = tiles[0]
                else:
                    result = tiles
                return [ t for t in result if t in self.hand ]
        else:
            return None


    @property
    def hand_and_melds(self):
        tiles = list(chain.from_iterable(self.melds))
        tiles.extend(self.hand)
        return TileCollection(tiles)


class AIPlayer(Player):
    def __init__(self, name=None):
        super().__init__(name=name)
        self.hints = True
        self.speed = 0.0

    def play_hint(self, game, hint, tiles):
        # call win if possible
        if hint == 'mahjongg':
            print('boom')
            import sys
            sys.exit()
            self.mahjongg(game, tiles)

        # meld random set if possible
        elif hint == 'sets':
            # TODO:
            #   Take the first for now.
            #   This could be changed to reflect AI personality.
            _set = tiles[0]

            logging.debug(f"Player {self.name} found set ({_set})")
            self.meld(game, _set)

    def take_turn(self, game):
        self.draw(game)

        # DO SOMETHING WITH TILES
        if self.hints:
            hint = game.get_hint_hand(self)
            if hint:
                logging.warn(f"{self.name} gets a hand hint: {hint}")
                self.play_hint(game, *hint)

        # discard best choice
        if self.hand:
            self.discard_best(game)
            assert len(self.hand_and_melds) == 13


class HumanPlayer(Player):
    def __init__(self, name=None):
        super().__init__(name=name)
        self.hints = True
        self.speed = 0.0

    def take_turn(self, game):
        # DO SOMETHING WITH TILES
        if self.hints:
            hint = game.get_hint_hand(self)
            if hint:
                logging.warn(f"{self.name} gets a hand hint: {hint}")

        # TODO: discard
        assert len(self.hand_and_melds) == 13
