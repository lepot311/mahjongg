import logging
import os
import time
import unicodedata

from termcolor import colored

from tiles import Basic, HonorTile, Wind


WINDS = list(Wind.create_tiles())

def get_wind(direction):
    return list(filter(lambda w: w.direction is direction, WINDS))[0]


def int_to_str(i):
    return {
        0: 'zero',
        1: 'one',
        2: 'two',
        3: 'three',
        4: 'four',
        5: 'five',
        6: 'six',
        7: 'seven',
        8: 'eight',
        9: 'nine',
    }[i]


def tile_to_unicode_name(tile):
    name = {
        'Character': unicode_name_basic,
        'Bamboo'   : unicode_name_basic,
        'Circle'   : unicode_name_basic,
        'Dragon'   : unicode_name_dragon,
        'Flower'   : unicode_name_flower,
        'Season'   : unicode_name_season,
        'Wind'     : unicode_name_wind,
    }[tile.__class__.__name__](tile)
    return f'MAHJONG TILE {name}'.upper()


def unicode_name_wind(tile):
    return f'{tile.direction} wind'


def unicode_name_dragon(tile):
    return f'{tile.color} dragon'


def unicode_name_basic(tile):
    return f'{int_to_str(tile.rank)} of {tile.__class__.__name__}s'

def unicode_name_season(tile):
    return tile.name

def unicode_name_flower(tile):
    return tile.name

def get_char(tile):
    name = tile_to_unicode_name(tile)
    return unicodedata.lookup(name)


class UnicodeRenderer:
    colors = [
        'red',
        'blue',
        'yellow',
        'green',
    ]

    def __init__(self, game):
        self.game = game

    def player_color(self, player):
        players = [ seat.player for seat in self.game.seats ]
        return dict(zip(players, self.colors))[player]

    def show_hands(self):
        for seat in self.game.seats:
            wind = get_char(get_wind(seat.direction))
            print()
            print(colored(wind, 'cyan'), end=' ')
            print(colored(seat.player.name, self.player_color(seat.player)), end=' ')
            print(colored(f"! " * seat.player.wins, 'cyan'))
            self.print_melds(seat.player)
            self.print_hand(seat.player, sort=True)

    def print_melds(self, player):
        for meld in player.melds:
            for tile in meld:
                t = get_char(tile)
                try:
                    other, discard = meld.called_from
                    color = self.player_color(other)
                    if tile is discard:
                        print(colored(t, color), end='')
                    else:
                        print(t, end='')
                except AttributeError:
                    print(t, end='')
            print(' ', end='')
        print()

    def print_hand(self, player, sort=False):
        h = player.hand
        if sort:
            h = player.hand.sorted()

        for t in h:
            print(get_char(t), end=' ')
        print()

    def update(self):
        os.system('clear')
        self.show_hands()

    def warn(self, msg):
        logging.warn(msg)

    def info(self, msg):
        logging.info(msg)
