from termcolor import colored

import ascii_tile


class AsciiRenderer:
    def __init__(self, game):
        self.game   = game

    def show_hands(self):
        for seat in self.game.seats:
            print()
            print(seat.player.name)
            print(f'    Wind: {seat.direction.capitalize()}')
            print()

            print(self.hand(seat.player))

    def hand(self, player):
        for t in player.hand:
            print(self.tile(t))

        tiles  = [ self.tile(t) for t in player.hand ]
        height = len(tiles[0])

        for row in range(height):
            for tile in tiles:
                print(str(tile[row]).strip(), end=' ')
            print()
        print()

    def tile(self, tile):
        return ascii_tile.draw(
            tile.__class__.__name__.lower(),
            **tile.__dict__,
        )
