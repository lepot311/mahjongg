import logging
from pprint import pprint
import os
import random
import time
import sys

from mahjong.exceptions import *
from mahjong.renderers.unicode import UnicodeRenderer


class Loop:
    def __init__(self, game):
        self.game = game

    def run(self):
        for r in range(self.game.n_rounds):
            self.game.create_round()
            logging.info(f'Round {self.game.round.number}')

            self.game.round.wash()

            print()

            for n in range(3):
                for player in self.game.players:
                    player.draw(self.game, count=4)

            for player in self.game.players:
                player.draw(self.game)

            while True:
                mahjonggs = []

                if not self.game.round.tiles:
                    logging.info(f"Round {self.game.round.number} ended without winner")
                    time.sleep(1)
                    break

                for player in self.game.players:
                    player.take_turn(self.game)

                    self.game.renderer.update()

                if mahjonggs:
                    winner = mahjonggs[0]

                    self.game.renderer.update()

                    logging.info(f"{winner.name} went MAHJONGG!")

                    break
