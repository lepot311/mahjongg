import abc
import itertools
import sys


class Tile(abc.ABC):
    code_map = {
        'b': 'bamboo',
        'c': 'circle',
        'd': 'dragon',
        'f': 'flower',
        'k': 'character',
        's': 'season',
        'w': 'wind',
    }

    @staticmethod
    def _from_code(code):
        rank, suit = code

        cls_name = __class__.code_map[suit].capitalize()
        cls = getattr(sys.modules[__name__], cls_name)

        # TODO improve this
        if cls in (Flower, Season, Dragon, Wind):
            try:
                name = [ n for n in cls.names
                         if n.startswith(rank) ][0]
            except:
                name = cls.names[int(rank) - 1]

            tile = cls(name)

        elif cls in (Bamboo, Character, Circle):
            tile = cls(rank=rank)

        return tile


    @classmethod
    def create_tiles(cls):
        pass

    @property
    def suit(self):
        return self.__class__.__name__.lower()


class Basic(Tile):
    copies = 4
    ranks = range(1, 9)

    def __repr__(self):
        return f'<{self.__class__.__name__}'\
               f'(rank={self.rank})>'

    def __str__(self):
        return f'{self.rank} {self.__class__.__name__.lower()}'

    @property
    def to_dict(self):
        return {
            'name': self.__class__.__name__,
            'rank': self.rank,
        }

    @classmethod
    def create_tiles(cls):
        return (
            cls(rank=rank)
            for rank in cls.ranks
            for n in range(cls.copies)
        )

    def __init__(self, rank=None):
        self.rank = int(rank)

    def __lt__(self, other):
        return self.rank < other.rank


class Character(Basic):
    short_name = 'crak'


class Bamboo(Basic):
    short_name = 'bam'


class Circle(Basic):
    short_name = 'dot'


class HonorTile(Tile):
    pass


class Dragon(HonorTile):
    copies = 4
    colors = (
        'green',
        'red',
        'white',
    )
    names = colors

    def __repr__(self):
        return f'<{self.__class__.__name__}'\
               f'(color={self.color})>'

    def __str__(self):
        return f'{self.color} {self.__class__.__name__.lower()}'

    @property
    def to_dict(self):
        return {
            'name': self.__class__.__name__,
            'color': self.color,
        }

    @classmethod
    def create_tiles(cls):
        return (
            Dragon(color=color)
            for color in cls.colors
            for n in range(cls.copies)
        )

    def __init__(self, color=None):
        self.color = color

    def __lt__(self, other):
        return self.color < other.color


class Wind(HonorTile):
    copies = 4
    directions = (
        'east',
        'north',
        'west',
        'south',
    )
    names = directions

    def __repr__(self):
        return f'<{self.__class__.__name__}'\
               f'(direction={self.direction})>'

    def __str__(self):
        return f'{self.direction} {self.__class__.__name__.lower()}'

    @property
    def to_dict(self):
        return {
            'name': self.__class__.__name__,
            'direction': self.direction,
        }

    @property
    def number(self):
        return self.directions.index(self.direction) + 1

    @classmethod
    def create_tiles(cls):
        return (
            Wind(direction=direction)
            for direction in cls.directions
            for n in range(cls.copies)
        )

    def __init__(self, direction=None):
        self.direction = direction

    def __lt__(self, other):
        return self.direction < other.direction


# TODO:
# Season and Flower are basically identical now

class Season(HonorTile):
    copies = 1
    names = (
        'winter',
        'spring',
        'summer',
        'autumn',
    )

    def __repr__(self):
        return f'<{self.__class__.__name__}'\
               f'(name={self.name})>'

    @property
    def to_dict(self):
        return {
            'name': self.__class__.__name__,
            'season': self.name,
        }

    @classmethod
    def create_tiles(cls):
        return (
            Season(name=name)
            for name in cls.names
            for n in range(cls.copies)
        )

    def __init__(self, name=None):
        self.name = name

    @property
    def number(self):
        return self.names.index(self.name) + 1

    def __lt__(self, other):
        return self.number < other.number

# TODO:
# Season and Flower are basically identical now

class Flower(HonorTile):
    copies = 1
    names = (
        'plum',
        'orchid',
        'bamboo',
        'chrysanthemum',
    )

    def __repr__(self):
        return f'<{self.__class__.__name__}'\
               f'(name={self.name})>'

    @property
    def to_dict(self):
        return {
            'name'  : self.__class__.__name__,
            'flower': self.name,
        }

    @classmethod
    def create_tiles(cls):
        return (
            Flower(name=name)
            for name in cls.names
            for n in range(cls.copies)
        )

    def __init__(self, name=None):
        self.name = name

    @property
    def number(self):
        return self.names.index(self.name) + 1

    def __lt__(self, other):
        return self.number < other.number
