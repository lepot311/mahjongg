import itertools


HEIGHT = 4
WIDTH  = 6


def tile(f):
    result = []
    result.append(edge())
    result.append(f())
    result.append(edge())
    return result

def edge():
    return ''.join(list(itertools.chain.from_iterable([
        [ '+' ],
        [ '-' for n in range(WIDTH) ],
        [ '+' ],
    ])))

def row(char):
    return ''.join((list(itertools.chain.from_iterable([
        [ '|' ],
        [ char for n in range(WIDTH) ],
        [ '|' ],
    ]))))

def body(char):
    return [ row(char) for n in range(HEIGHT) ]

@tile
def blank_tile():
    return body(' ')

def draw(suit, rank=None, color=None, direction=None):
    return blank_tile

def tile_circle(tile):
    return blank_tile()

def tile_bamboo(tile):
    return blank_tile()

def tile_dragon(tile):
    t = '''
    +------+
    |      |
    |  {}  |
    |  {}  |
    |      |
    +------+
    '''.format('D', 'D')
    return t

def tile_flower(tile):
    t = '''
    +------+
    |    {}|
    |  {}  |
    |  {}  |
    |      |
    +------+
    '''.format(tile.number, 'F', 'F')
    return t

def tile_wind(tile):
    t = '''
    +------+
    |    {}|
    |  {}  |
    |  {}  |
    |      |
    +------+
    '''.format(tile.number, 'W', 'W')
    return t

def tile_season(tile):
    t = '''
    +------+
    |    {}|
    |  {}  |
    |  {}  |
    |      |
    +------+
    '''.format(tile.number, 'S', 'S')
    return t

