from itertools import chain
import logging
from pathlib import Path
import pickle
import random
import uuid

from mahjong.exceptions import *
from mahjong.tiles import *
from mahjong.tilecollection import *
from mahjong.loop import Loop


class Round:
    def __init__(self, game):
        self.game      = game
        self.dealer    = None
        self.wind      = None
        self.discards  = []
        self.mahjonggs = []
        self.tiles     = []


    def wash(self):
        self.tiles = random.sample(self.game.tiles, k=len(self.game.tiles))
        self.game.renderer.info('Washed tiles')


    @property
    def number(self):
        return self.game.rounds.index(self)


    @property
    def last_discard(self):
        try:
            return self.discards[-1]
        except IndexError:
            return None


class Seat:
    def __init__(self, direction=None):
        self.direction = direction
        self.player    = None


    @property
    def is_empty(self):
        return not self.player


class Game:
    n_rounds = 4

    tiles = list(itertools.chain.from_iterable([
        cls.create_tiles()
        for cls in [
            Character,
            Bamboo,
            Circle,
            Dragon,
            Wind,
            Season,
            Flower,
        ]
    ]))


    def __init__(self):
        self.players = set()
        self.active_player = None
        self.rounds  = []
        self.seats   = [
            Seat(direction=direction)
            for direction in Wind.directions
        ]
        self.renderer = None
        self.loop = Loop(self)
        self.uuid = uuid.uuid4()
        self.pickle_path = Path("pickles") / str(self.uuid)


    @property
    def state(self):
        result = {}

        result['seats'] = [
            {
                'player'   : seat.player.to_dict,
                'direction': seat.direction,
            } for seat in self.seats if seat.player
        ]

        result['tiles'] = len(self.round.tiles)

        try:
            result['active_seat'] = self.active_player.seat(self)
        except AttributeError:
            result['active_seat'] = None

        if self.round.mahjonggs:
            result['mahjongg'] = self.round.mahjonggs[0].seat(self)

        return result


    @property
    def round(self):
        if not self.rounds:
            self.create_round()
        return self.rounds[-1]


    @property
    def can_run(self):
        return len(self.players) == 4


    def create_round(self):
        # pickle the game for rollbacks
        with self.pickle_path.open('wb') as fh:
            pickle.dump(self, fh)

        for player in self.players:
            self.player_reset_hand(player)
        self.rounds.append(Round(self))


    def check_mahjongg_call(self, player):
        tiles = player.hand + [self.round.last_discard]

        winning_hand = set()

        for s in tiles.sets:
            if set(s).isdisjoint(winning_hand):
                winning_hand.update(s)

        for pair in tiles.pairs:
            if set(pair).isdisjoint(winning_hand):
                winning_hand.update(pair)

        winning_hand = TileCollection(winning_hand)

        return (
            len(winning_hand.pairs) == 1
            and len(winning_hand.sets) >= 3
        )


    def check_mahjongg_meld(self, player):
        '''
        Returns True if the player has a mahjongg
        '''
        return (
            player.hand_and_melds.mahjongs
        )


    def get_sets(self, player, tiles):
        '''
        Returns the sets in the players hand
        '''
        return tiles.sets or []


    def get_hint_call(self, player):
        if self.check_mahjongg_call(player):
            logging.warn(f"{player.name} mahjongg with {player.hand} and discard {self.round.last_discard})")
            return ('mahjongg', player.hand + [self.round.last_discard])

        else:
            sets = self.get_sets(player, player.hand + [self.round.last_discard])

            # remove illegal calls
            for s in sets:
                # chow from left?
                if isinstance(s, Chow):
                    try:
                        if self.round.last_discard.discarded_by.seat(self) != player.seat(self) - 1:
                            # illegal chow
                            sets.remove(s)
                    except AttributeError:
                        pass

            return ('sets', [ s for s in sets if self.round.last_discard in s ])


    def get_hint_hand(self, player):
        '''
        Returns a tuple of (string, tiles[]) representing the best play that
        can be made based on the player's current hand.
        '''
        if self.check_mahjongg_meld(player):
            logging.warn(f"{player.name} mahjongg with {player.hand} and {player.melds})")
            return ('mahjongg', player.hand)

        else:
            sets = self.get_sets(player, player.hand)
            if sets:
                return ('sets', sets)
            else:
                return None


    def player_draw(self, player, count):
        '''
        Draws {count} number of tiles from the current round's available tiles
        and appends them to this player's hand.
        '''
        for n in range(count):
            # TODO: try/except if we can't pop any more tiles from the round
            tile = self.round.tiles.pop()

            player.hand.append(tile)

            msg = f"{player.name} drew a tile"
            self.renderer.info(msg)


    def player_discard(self, player, tile):
        player.hand.remove(tile)
        player.discards.append(tile)
        tile.discarded_by = player
        self.round.discards.append(tile)
        self.renderer.info(f'{player.name} discarded {tile}')
        self.renderer.update()


    def player_meld(self, player, tiles):
        for tile in tiles:
            try:
                player.hand.remove(tile)
            except ValueError:
                raise BadDiscard(f"Tile {tile} isn't in player's hand.")
        player.melds.append(tiles)
        self.renderer.info(f'{player.name} melded {tiles}')


    def player_take_tile(self, player, other, tile):
        self.renderer.warn(f"Taking {tile} from {other.name}")

        other.discards.remove(tile)
        assert tile not in other.discards

        player.hand.append(tile)
        assert tile in player.hand

        tile.taken_from = other


    def player_call_meld(self, player, tiles, discard):
        meld = Meld._from(TileCollection(tiles + [discard]))
        taken_from = [ p for p in self.players if not p is player
                                               and discard in p.discards ][0]

        self.renderer.warn(f"{player.name} called meld of {tiles} and {repr(discard)}")

        self.player_take_tile(player, taken_from, discard)

        for tile in meld:
            player.hand.remove(tile)

        player.melds.append(meld)
        self.renderer.warn(f"{player.name} completed meld of {meld}")


    def player_mahjongg(self, player, tiles):
        player.melds.append(tiles)
        player.hand = TileCollection()
        self.round.mahjonggs.append(player)


    def player_reset_hand(self, player):
        player.hand     = TileCollection()
        player.discards = TileCollection()
        player.melds    = []


    def player_fold(self, player):
        player.hand     = TileCollection()
        player.melds    = []
        self.renderer.info(f"{player.name} folded")


    def player_join(self, player):
        if self.full:
            return False
        else:
            self.players.add(player)
            empty = [ s for s in self.seats if s.is_empty ]
            seat  = random.choice(empty)
            seat.player = player
            self.renderer.info(f'{player.name} joined game {self}')


    def player_leave(self, player):
        self.seats.remove(player)


    @property
    def full(self):
        return not any([ s.is_empty for s in self.seats ])


    def render(self):
        self.renderer.show_hands()
