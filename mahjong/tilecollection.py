from collections import defaultdict, UserList
from itertools import chain, combinations, permutations
import logging

from mahjong.tiles import Tile


class TileCollection(UserList):
    @classmethod
    def from_str(cls, codes):
        if isinstance(codes, str):
            codes = codes.split()
        return cls([ Tile._from_code(code) for code in codes ])

    @property
    def to_dict(self):
        return {
            'tiles': [ t.to_dict for t in self ],
        }

    @property
    def sets(self):
        return list(chain(self.kongs, self.pungs, self.chows))

    @property
    def kongs(self):
        return self.sets_of(Kong)

    @property
    def pungs(self):
        return self.sets_of(Pung)

    @property
    def chows(self):
        return self.sets_of(Chow)

    @property
    def pairs(self):
        return self.sets_of(Pair)

    @property
    def mahjongs(self):
        sets = self.melds
        sets.extend(self.pairs)
        return TileCollection.find_mahjongs(sets)

    @staticmethod
    def find_mahjongs(melds):
        '''
        Do a sanity check on the melds.
        If that passes, get all possible orderings of the melds.
        For each one, pull elements off until it's a mahjong.
        '''
        # sanity check
        if not len(list(chain.from_iterable(melds))) >= Mahjong.min_length:
            return []

        result = []
        for n in range(len(melds) + 1):
            mahjongs = [
                c for c in combinations(melds, n)
                if Mahjong.is_valid(c)
            ]
            result.extend(mahjongs)
        return result

    @property
    def melds(self):
        return list(chain.from_iterable([ self.sets_of(cls) for cls in (Kong, Pung, Chow) ]))

    def sets_of(self, cls):
        result = []
        for suit in self.suits:
            for combo in combinations(suit, cls.length):
                # TODO could make this a try/except
                tiles = TileCollection(combo).sorted()
                if cls.is_valid(tiles):
                    result.append(cls(tiles))
        return result

    @property
    def suits(self):
        return self.tiles_by_suit.values()

    @property
    def tiles_by_suit(self):
        groups = defaultdict(list)
        # group tiles by suit
        for tile in self:
            groups[tile.__class__.__name__].append(tile)
        return groups

    @property
    def unused(self):
        return [ t for t in self if (not t in chain(*self.sets)
                                 and not t in chain(*self.pairs)) ]

    @property
    def is_match(self):
         return (self.is_same_rank
              or self.is_same_direction
              or self.is_same_color)

    @property
    def is_unique(self):
        return len(self) == len(set(self))

    @property
    def is_consecutive(self):
        if not hasattr(self[0], 'rank'):
            return False

        return (self[0].rank + 1 == self[1].rank
            and self[1].rank + 1 == self[2].rank)

    def is_same(self, attr):
        try:
            _attr = getattr(self[0], attr)
        except AttributeError:
            return False
        else:
            return all([ getattr(tile, attr) == _attr for tile in self ])

    @property
    def is_same_suit(self):
        return self.is_same('suit')

    @property
    def is_same_rank(self):
        return self.is_same('rank')

    @property
    def is_same_direction(self):
        return self.is_same('direction')

    @property
    def is_same_color(self):
        return self.is_same('color')

    def sorted(self):
        result = []
        types = self.tiles_by_suit

        for name in sorted(types.keys()):
            group = types[name]
            result.append(sorted(group))

        return TileCollection(chain(*result))


class Meld(TileCollection):
    @classmethod
    def is_valid(cls, tiles):
        tiles = tiles.sorted()
        for cls in Kong, Pung, Chow:
            if cls.is_valid(tiles):
                return cls(tiles)


class Kong(Meld):
    length = 4

    @classmethod
    def is_valid(cls, tiles):
        return (
            len(tiles) == cls.length
            and tiles.is_unique
            and tiles.is_same_suit
            and tiles.is_match
        )


class Pung(Meld):
    length = 3

    @classmethod
    def is_valid(cls, tiles):
        return (
            len(tiles) == cls.length
            and tiles.is_unique
            and tiles.is_same_suit
            and tiles.is_match
        )


class Chow(Meld):
    length = 3

    @classmethod
    def is_valid(cls, tiles):
        return (
            len(tiles) == cls.length
            and tiles.is_unique
            and tiles.is_same_suit
            and tiles.is_consecutive
        )


class Pair(Meld):
    length = 2

    @classmethod
    def is_valid(cls, tiles):
        return (
            len(tiles) == cls.length
            and tiles.is_unique
            and tiles.is_same_suit
            and tiles.is_match
        )


class Mahjong(TileCollection):
    min_length = 14

    @classmethod
    def is_valid(cls, melds):
        '''
        This is a little different than the other validators in that it works
        on a list of melds instead of a list of tiles.
        '''
        tiles = TileCollection(chain.from_iterable(melds))
        return (
            tiles.is_unique
            # each meld is of a valid type
            and all([ m.__class__ in (Kong, Pung, Chow, Pair) for m in melds ])
            # the total number of tiles is at least some number
            and len(tiles) >= cls.min_length
            # there is a single pair
            and len([ m for m in melds if m.__class__ == Pair ]) == 1
        )
