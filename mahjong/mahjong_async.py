import asyncio
import logging
from pprint import pprint
import os
import random
import time
import sys


from game import Game, Round
from player import AIPlayer
from exceptions import *
from renderer_unicode import UnicodeRenderer
from loop_async import loop as aloop


logging.basicConfig(
    filename="mahjong_async.log",
    level=logging.INFO,
)


def make_game(number):
    game = Game()
    game.renderer = UnicodeRenderer(game)
    game.number = number

    players = [
        AIPlayer(name=name)
        for name in [
            'Jess',
            'Jon',
            'Bethany',
            'Erik',
        ]
    ]

    for player in players:
        player.join(game)

    game.loop = aloop
    return game


if __name__ == '__main__':
    games = [ make_game(n) for n in range(1) ]

    loop = asyncio.get_event_loop()

    game_loops = [ game.loop(game) for game in games ]
    group = asyncio.wait(game_loops, return_when=asyncio.FIRST_COMPLETED)
    loop.run_until_complete(group)
