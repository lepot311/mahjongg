Python 3.6 or higher required

Log file: mahjongg.log

Installation
------------
git clone git@bitbucket.org:lepot311/mahjongg.git
cd mahjongg/
python3.6 -m venv env
. env/bin/activate
pip install -r requirements.pip

Usage
-----
python mahjongg.py 
