import asyncio
import logging

from exceptions import *
from player import AIPlayer, HumanPlayer


log = logging.getLogger(__name__)


async def loop(game):
    for r in range(game.n_rounds):
        while not game.can_run:
            log.warn('Waiting to run')
            await asyncio.sleep(1)

        game.create_round()
        game.renderer.warn(f'Round {game.round.number}')

        game.round.wash()

        for n in range(3):
            for player in game.players:
                log.info(f"{player.name} is drawing four tiles for initial hand.")
                player.draw(game, count=4)
                await asyncio.sleep(.05)
                game.renderer.update()

        for player in game.players:
            log.info(f"{player.name} is drawing one tile for initial hand.")
            player.draw(game)
            await asyncio.sleep(.05)
            game.renderer.update()

            assert len(player.hand) == 13


        while True:
            game.round.mahjonggs = []

            if not game.round.tiles:
                game.renderer.warn(f"Round {game.round.number} ended without winner")
                await asyncio.sleep(8)
                break

            for player in game.players:
                game.active_player = player

                assert len(player.hand_and_melds) == 13
                player.draw(game)
                game.renderer.update()
                assert len(player.hand_and_melds) == 14

                # player takes their turn
                await server_loop.message

                await asyncio.sleep(.05)
                game.renderer.update()

                # other players get to call melds
                calls = []
                others = [ p for p in game.players if not p is player ]

                for other in others:
                    call = other.get_calls(game)
                    if call:
                        log.warn(f'got call {call}')
                        calls.append((other, call))

                if calls:
                    log.warn(f"got {len(calls)} calls")
                    log.warn(f"calls:\n{calls}")

                    # TODO: not taking into account tile ranks
                    for other, tiles in calls:
                        try:
                            other.call_meld(game, tiles, game.round.last_discard)
                        except MeldCallError:
                            continue
                        else:
                            if other.hand:
                                other.discard_best(game)
                            break

                game.renderer.update()

            if game.round.mahjonggs:
                winner = game.round.mahjonggs[0]
                winner.wins += 1

                game.renderer.warn(f"{player.name} went MAHJONGG!")
                game.renderer.update()

                await asyncio.sleep(8)
                break
