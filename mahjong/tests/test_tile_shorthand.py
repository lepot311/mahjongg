import unittest

from mahjong.tiles import Tile


class TileShorthandTest(unittest.TestCase):
    '''
    Each combination of tile rank/suit should be able to be created using the
    corresponding shorthand.
    '''
    def test_shorthand_tile_bamboo(self):
        for n in range(9):
            tile = Tile._from_code(f"{n+1}b")
            self.assertEqual(tile.rank, n+1)
            self.assertEqual(tile.__class__.__name__, 'Bamboo')

    def test_shorthand_tile_character(self):
        for n in range(9):
            tile = Tile._from_code(f"{n+1}k")
            self.assertEqual(tile.rank, n+1)
            self.assertEqual(tile.__class__.__name__, 'Character')

    def test_shorthand_tile_circle(self):
        for n in range(9):
            tile = Tile._from_code(f"{n+1}c")
            self.assertEqual(tile.rank, n+1)
            self.assertEqual(tile.__class__.__name__, 'Circle')

    def test_shorthand_tile_dragon(self):
        for c, color in zip(('g', 'r', 'w'),
                            ('green', 'red', 'white')):
            tile = Tile._from_code(f"{c}d")
            self.assertEqual(tile.color, color)
            self.assertEqual(tile.__class__.__name__, 'Dragon')

    def test_shorthand_tile_wind(self):
        for d, direction in zip(('e', 's', 'w', 'n'),
                                ('east', 'south', 'west', 'north')):
            tile = Tile._from_code(f"{d}w")
            self.assertEqual(tile.direction, direction)
            self.assertEqual(tile.__class__.__name__, 'Wind')

    def test_shorthand_tile_flower(self):
        for n, name in zip((1, 2, 3, 4),
                             ('plum',
                              'orchid',
                              'bamboo',
                              'chrysanthemum',
                             )):
            tile = Tile._from_code(f"{n}f")
            self.assertEqual(tile.name, name)
            self.assertEqual(tile.__class__.__name__, 'Flower')

    def test_shorthand_tile_season(self):
        for n, name in zip((1, 2, 3, 4),
                             ('winter',
                              'spring',
                              'summer',
                              'autumn',
                             )):
            tile = Tile._from_code(f"{n}s")
            self.assertEqual(tile.name, name)
            self.assertEqual(tile.__class__.__name__, 'Season')

