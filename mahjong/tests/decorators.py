from functools import wraps

from mahjong.tilecollection import TileCollection


def hand(tiles):
    def wrapper(f):
        @wraps(f)
        def wrapped(self):
            self.hand = TileCollection.from_str(tiles)
        return wrapped
    return wrapper
