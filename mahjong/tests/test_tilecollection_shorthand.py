import unittest

from decorators import hand
from mahjong.tiles import Tile


class TileCollectionShorthandTest(unittest.TestCase):
    @hand('1b 2b 3b 4b 5b 6b 7b 8b 9b wd wd wd ew')
    def test_collection_shorthand_instance(self):
        self.assertTrue(isinstance(self.hand, TileCollection))

    @hand('1b 2b 3b 4b 5b 6b 7b 8b 9b wd wd wd ew')
    def test_collection_shorthand_length(self):
        self.assertEqual(len(self.hand), 13)

    @hand('1b 2b 3b 4b 5b 6b 7b 8b 9b wd wd wd ew')
    def test_collection_shorthand_membership(self):
        for i, tile in enumerate(self.hand[:9]):
            self.assertEqual(tile.rank, i+1)
            self.assertEqual(tile.__class__.__name__, 'Bamboo')

        for i, tile in enumerate(self.hand[9:12]):
            self.assertEqual(tile.color, 'white')
            self.assertEqual(tile.__class__.__name__, 'Dragon')

        self.assertEqual(self.hand[-1].direction, 'east')
        self.assertEqual(self.hand[-1].__class__.__name__, 'Wind')
