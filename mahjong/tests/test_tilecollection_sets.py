import unittest

from mahjong.tilecollection import TileCollection


class TileCollectionSetTest(unittest.TestCase):
    def test_collection_has_sets(self):
        hand = TileCollection.from_str('1b 2b 3b 4b 5b 6b 7b 8b 9b')
        self.assertTrue(hand.sets)

    def test_collection_sets_length(self):
        hand = TileCollection.from_str('1b 2b 3b 4b 5b 6b 7b 8b 9b')
        self.assertEqual(len(hand.sets), 7)


class TileCollectionChowTest(unittest.TestCase):
    def test_collection_has_chows(self):
        hand = TileCollection.from_str('1b 2b 3b 4b 5b 6b 7b 8b 9b')
        self.assertTrue(hand.chows)

    def test_collection_chows_length(self):
        hand = TileCollection.from_str('1b 2b 3b 4b 5b 6b 7b 8b 9b')
        self.assertEqual(len(hand.chows), 7)


class TileCollectionPungTest(unittest.TestCase):
    def test_collection_has_pungs(self):
        hand = TileCollection.from_str('1b 1b 1b 1c 1c 1c 1k 1k 1k')
        self.assertTrue(hand.pungs)

    def test_collection_pungs_length(self):
        hand = TileCollection.from_str('1b 1b 1b 1c 1c 1c 1k 1k 1k')
        self.assertEqual(len(hand.pungs), 3)


class TileCollectionKongTest(unittest.TestCase):
    def test_collection_has_kongs(self):
        hand = TileCollection.from_str('1b 1b 1b 1b 1c 1c 1k 1k 1k')
        self.assertTrue(hand.kongs)

    def test_collection_kongs_length(self):
        hand = TileCollection.from_str('1b 1b 1b 1b 1c 1c 1k 1k 1k')
        self.assertEqual(len(hand.kongs), 1)


class TileCollectionMahjongTest(unittest.TestCase):
    def test_collection_has_mahjongs(self):
        hand = TileCollection.from_str('1b 2b 3b 1c 2c 3c 1k 2k 3k rd rd rd ew ew')
        self.assertTrue(hand.mahjongs)

    def test_collection_has_no_mahjongs(self):
        hand = TileCollection.from_str('1b 2b 3b 1c 2c 3c 1k 2k 3k rd rd rd ew')
        self.assertFalse(hand.mahjongs)

    def test_collection_mahjongs_length(self):
        hand = TileCollection.from_str('1b 2b 3b 1c 2c 3c 1k 2k 3k rd rd rd ew ew')
        self.assertEqual(len(hand.mahjongs), 1)
