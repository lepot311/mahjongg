import asyncio
import json
import logging

import websockets
from websockets.exceptions import ConnectionClosed

from mahjong.game import Game
from mahjong.player import AIPlayer, HumanPlayer
from mahjong.loop_async import loop as aloop
from mahjong.renderers.websocket import WebsocketRenderer


logging.basicConfig(
    filename="server.log",
    level=logging.INFO,
)

log = logging.getLogger(__name__)


clients = set()
games   = dict()
players = []


async def broadcast(msg):
    for client in clients:
        await client.send(msg)


def getState(game):
    return {
        'event': 'state',
        'data' : game.state,
    }


def onChat(data, websocket):
    msg = {
        'event': 'chat',
        'data' : {
            'player': 'Abilene',
            'line'  : data,
        },
    }
    asyncio.ensure_future(broadcast(json.dumps(msg, ensure_ascii=False)))


def echo(game, event):
    asyncio.ensure_future(broadcast(json.dumps(event, ensure_ascii=False)))


def gameCallback(game, event):
    echo(game, event)


async def onJoin(data, websocket):
    player_name = data['player']
    game_name   = data['game']

    player = HumanPlayer(player_name)
    log.info(f"Player {player} requests game {game_name}")

    if not game_name in games:
        # create new game
        game = Game()
        game.renderer = WebsocketRenderer(game, gameCallback)
        game.loop = aloop
        games[game_name] = game

    game = games[game_name]

    log.info(f"Player {player} joining game {game}")
    player.join(game)
    await websocket.send(json.dumps({
        'seat': player.seat(game),
    }))

    for n in range(3):
        p = AIPlayer(f"AIPlayer {n+1}")
        p.join(game)

    asyncio.ensure_future(broadcast(json.dumps(getState(game))))
    asyncio.ensure_future(game.loop(game))

def onAction(data, websocket):
    log.info(f"Received {data['event']} action from {data['player']}")


events = {
    'chat'  : onChat,
    'join'  : onJoin,
    'action': onAction,
}


async def recv_handler(websocket):
    '''
    Takes care of receiving all messages coming from clients.
    '''
    data = await websocket.recv()

    # naively protect against large payloads
    if len(data) > 512:
        raise

    log.info(data)

    msg = json.loads(data)

    await events[msg['event']](msg['data'], websocket)

    await asyncio.sleep(1)


async def count_handler(websocket, n):
    msg = {
        'event': 'ping',
        'data' : n,
    }
    await websocket.send(json.dumps(msg))
    await asyncio.sleep(1)


async def handler(websocket, path):
    global clients
    global games
    global players

    clients.add(websocket)
    log.info(f"Client connected: {websocket}")

    count = 0

    while websocket.open:
        task_recv  = asyncio.ensure_future(recv_handler(websocket))
        #task_count = asyncio.ensure_future(count_handler(websocket, count))

        done, pending = await asyncio.wait([
                task_recv,
                #task_count,
            ],
            return_when=asyncio.FIRST_COMPLETED,
        )

        for task in pending:
            task.cancel()

        count += 1

    for task in pending:
        task.cancel()
    clients.remove(websocket)


if __name__ == '__main__':
    server = websockets.serve(handler, None, 3000)

    loop = asyncio.get_event_loop()

    loop.run_until_complete(server)
    loop.run_forever()

    log.shutdown()
