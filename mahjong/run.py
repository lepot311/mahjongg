import logging
from pprint import pprint
import os
import random
import time
import sys


from mahjong.game import Game, Round
from mahjong.player import AIPlayer
from mahjong.exceptions import *
from mahjong.renderers.unicode import UnicodeRenderer


logging.basicConfig(
    filename="mahjong.log",
    level=logging.DEBUG,
)


if __name__ == '__main__':
    game = Game()
    game.renderer = UnicodeRenderer(game)

    players = [
        AIPlayer(name=name)
        for name in [
            'Jess',
            'Jon',
            'Bethany',
            'Erik',
        ]
    ]

    for player in players:
        player.join(game)

    game.loop.run()
