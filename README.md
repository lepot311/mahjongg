Mahjong
===============================

version number: 0.0.1
author: Erik Potter

Overview
--------

A mahjong game

Installation / Usage
--------------------

To install use pip:

    $ pip install mahjong


Or clone the repo:

    $ git clone https://github.com/lepot311/mahjong.git
    $ python setup.py install
    
Contributing
------------

TBD

Example
-------

TBD